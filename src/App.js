//Libs
import React, { Component } from 'react';

// Css/Scss
import './App.css';

// Components
import TopBar from './Components/TopBar/TopBar'
import Menu from './Components/Menu/Menu.js';
import Hero from './Components/Hero/Hero';
import Content from './Components/Content/Content';
import CardContainer from './Components/Card/CardContainer';
import Cta from './Components/Cta/Cta';
import Contact from './Components/Contact/Contact';
import Footer from './Components/Footer/Footer';
import FooterSecondary from './Components/FooterSecondary/FooterSecondary';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <TopBar/>
        <Menu/>
        <Hero/>
        <Content/>
        <CardContainer/>
        <Cta/>
        <Contact/>
        <Footer/>
        <FooterSecondary/>
      </React.Fragment>
    );
  }
}

export default App;
