import React from 'react';
import { Link } from 'react-router-dom';

class ContentContainer extends React.Component{

  render() {
    return (
      <React.Fragment>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin dictum risus enim, a sodales elit faucibus eget. Vestibulum rhoncus lorem nec vehicula dapibus. Proin in augue consequat, fermentum nisl et, vehicula purus. Maecenas ac diam mi. Morbi iaculis pretium gravida. Phasellus gravida urna ante, in semper augue rhoncus at. Ut tristique quam nec urna ornare, ac fringilla odio fringilla.</p>
        <p>Suspendisse sed tempus turpis. Pellentesque turpis est, consectetur eget tellus a, euismod egestas urna. Donec nec ex in nisl dictum pellentesque at aliquam leo. In molestie placerat imperdiet. Nulla felis libero, eleifend nec maximus et, egestas ultrices massa. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean feugiat metus arcu, ut consectetur ante accumsan sit amet. Maecenas ullamcorper laoreet lacus at feugiat. Duis posuere, magna sed viverra pharetra, urna orci sodales nibh, nec efficitur neque erat vel sem. Vivamus vel malesuada diam, sed vestibulum eros. Vivamus consectetur ex eros, eleifend ornare elit condimentum eget. Curabitur et pellentesque nulla.</p>
        <div className="responsive-embed">
          <iframe allow="autoplay; encrypted-media" allowFullScreen className="responsive-embed__item" frameBorder={0} src="https://www.youtube.com/embed/SpQivWJEi58" title="youtube"/>
        </div>
      </React.Fragment>
    );
  }
}

export default ContentContainer;
