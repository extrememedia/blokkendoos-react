import React from 'react';
// import { Link } from 'react-router-dom';

class ContentMenu extends React.Component{

  render() {
    return (
      <ul className="list">
        <li className="list__item">
          <a className="link link--block link--color-text link--padding" href="#" title="Pagina 1.1">Pagina 1.1</a>
          <ul className="list list--no-margin text-small">
            <li className="list__item">
              <a className="link link--block link--color-text link--padding" href="#" title="Pagina 1.1">Pagina 1.1.1</a>
            </li>
            <li className="list__item">
              <a className="link link--block link--color-text link--padding" href="#" title="Pagina 1.2">Pagina 1.1.2</a>
            </li>
            <li className="list__item">
              <a className="link link--block link--color-text link--padding" href="#" title="Pagina 1.3">Pagina 1.1.3</a>
            </li>
          </ul>
        </li>
        <li className="list__item">
          <a className="link link--block link--color-text link--padding" href="#" title="Pagina 1.2">Pagina 1.2</a>
        </li>
        <li className="list__item">
          <a className="link link--block link--color-text link--padding" href="#" title="Pagina 1.3">Pagina 1.3</a>
        </li>
      </ul>
    );
  }
}

export default ContentMenu;
