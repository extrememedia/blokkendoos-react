import React from 'react';
import { Link } from 'react-router-dom';
import ContentMenu from './ContentMenu';
import ContentContainer from './ContentContainer';

class Content extends React.Component{

  render() {
    return (
      <div className="section">
        <div className="container">
          <div className="grid grid--1-5-1">
            <div className="grid__item grid__item--center">
              <ContentContainer/>
            </div>
            <div className="grid__item grid__item--left">
              <ContentMenu/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Content;
