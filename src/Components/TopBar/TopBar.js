import React from 'react';
import { Link } from 'react-router-dom';
import LanguageSwitch from './LanguageSwitch';

class TopBar extends React.Component{

  render() {
    return (
      <div className="language-bar bar--contrast hidden-for-max-tablet-landscape">
        <div className="container">
          <LanguageSwitch/>
        </div>
      </div>
    );
  }
}

export default TopBar;
