import React from 'react';
import { Link } from 'react-router-dom';

class LanguageSwitch extends React.Component{

  render() {
    return (
      <React.Fragment>
        <ul className="language-switch">
          <li className="language-switch__item">
            <a className="language-switch__link" href="#" title="nl">NL</a>{/*link--color-text link--no-decoration*/}
          </li>
          <li className="language-switch__item">
            <a className="language-switch__link" href="#" title="en">EN</a>{/*link--color-text link--no-decoration*/}
          </li>
        </ul>
      </React.Fragment>
    );
  }
}

export default LanguageSwitch;
