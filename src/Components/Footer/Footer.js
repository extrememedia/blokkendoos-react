import React from 'react';
import { Link } from 'react-router-dom';
import FacebookIcon from './img/facebook-logo-button.svg';
import TwitterIcon from './img/twitter-logo-button.svg';
import FooterInfo from './FooterInfo';
import FooterItem from './FooterItem';
import FooterSubscribe from './FooterSubscribe';

class Footer extends React.Component{

  render() {
    return (
      <div className="footer">
        <div className="footer__primary">
          <div className="container">
            <div className="grid grid--4-for-min-tablet-landscape">
              <FooterInfo/>
              <FooterItem/>
              <FooterItem/>
              <FooterSubscribe/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
