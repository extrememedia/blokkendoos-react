import React from 'react';
import { Link } from 'react-router-dom';
import FacebookIcon from './img/facebook-logo-button.svg';
import TwitterIcon from './img/twitter-logo-button.svg';

class FooterInfo extends React.Component{

  render() {
    return (
      <div className="grid__item">
        <h3>Bedrijfsnaam</h3>
        <p>
          De Schans 1802<br />
          8231 KA Lelystad<br />
          T. +31 320 22 35 10<br />
          E. info@bedrijfsnaam.nl
        </p>
        <ul className="footer__social-media">
          <li className="list__item">
            <a className="link " href="#"><img src={FacebookIcon} alt="facebook"/></a>
          </li>
          <li className="list__item">
            <a className="link" href="#"><img src={TwitterIcon} alt="twitter"/></a>
          </li>
        </ul>
      </div>
    );
  }
}

export default FooterInfo;
