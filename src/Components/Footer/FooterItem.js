import React from 'react';
// import { Link } from 'react-router-dom';

class FooterItem extends React.Component{

  render() {
    return (
      <div className="grid__item">
        <h3>Lorem ipsum dolor</h3>
        <ul className="list">
          <li className="list__item">
            <a className="link" href="#">Lorem</a>
          </li>
          <li className="list__item">
            <a className="link" href="#">Ipsum</a>
          </li>
          <li className="list__item">
            <a className="link" href="#">Dolor</a>
          </li>
        </ul>
      </div>
    );
  }
}

export default FooterItem;
