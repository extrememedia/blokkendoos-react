import React from 'react';
import { Link } from 'react-router-dom';

class FooterSubscribe extends React.Component{

  render() {
    return (
      <div className="grid__item">
        <h3>Blijf op de hoogte</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        <div className="button-input">
          <input className="input" placeholder="E-mailadres" type="text" aria-label="Email" />
          <button className="button" type="submit">Inschrijven</button>
        </div>
      </div>
    );
  }
}

export default FooterSubscribe;
