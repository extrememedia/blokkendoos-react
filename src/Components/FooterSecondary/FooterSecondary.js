import React from 'react';
// import { Link } from 'react-router-dom';

class FooterSecondary extends React.Component{

  render() {
    return (
      <div className="footer__secondary">
        <div className="container">
          <ul className="footer__legal">
            <li className="list__item">
              <a className="link" href="#" title="Privacyverklaring">Privacyverklaring</a>
            </li>
            <li className="list__item">
              <a className="link" href="#" title="Disclaimer">Disclaimer</a>
            </li>
            <li className="list__item">
              <a className="link" href="#" title="Voorwaarden">Voorwaarden</a>
            </li>
          </ul>
          <div>© 2018 Bedrijfsnaam</div>
        </div>
      </div>
    );
  }
}

export default FooterSecondary;
