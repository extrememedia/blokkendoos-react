import React from 'react';
import { Link } from 'react-router-dom';

class NavbarSideHeader extends React.Component{

  render() {
    return (
        <div className="menu__mobile-header">
          <button className="menu__close-mobile-menu" type="button">🞩</button>
        </div>
    );
  }
}

export default NavbarSideHeader;
