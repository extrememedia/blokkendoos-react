import React from 'react';
import { Link } from 'react-router-dom';
import MenuList from '../MenuList'
import LanguageSwitch from '../../TopBar/LanguageSwitch';
import MenuSearch from '../MenuSearch';
import MenuSideHeader from './MenuSideHeader';

class NavbarSide extends React.Component{

  render() {
    return (
      <div className="menu__mobile">
        <MenuSideHeader/>
        <MenuSearch/>
        <MenuList/>
        <LanguageSwitch/>
      </div>
    );
  }
}

export default NavbarSide;
