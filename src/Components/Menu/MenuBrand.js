import React from 'react';
import { Link } from 'react-router-dom';
import logo from './img/logo.svg';

class MenuBrand extends React.Component{

  render() {
    return (
      <a className="menu__logo" href="#" title="#"><img src={logo} className="logo__image" alt="logo" /></a>
    );
  }
}

export default MenuBrand;
