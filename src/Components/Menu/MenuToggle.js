import React from 'react';
import { Link } from 'react-router-dom';

class MenuToggle extends React.Component{

  render() {
    return (
      <button className="menu__toggle-mobile-menu" type="button"><p className="button__icon">&#9776;</p></button>
    );
  }
}

export default MenuToggle;
