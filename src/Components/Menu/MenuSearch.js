import React from 'react';
import { Link } from 'react-router-dom';

class MenuSearch extends React.Component{

  render() {
    return (

      <div className="menu__mobile-search">
        <div className="button-input">{/*button-input--border*/}
          <input className="input" placeholder="Zoeken..." type="text" aria-label="Zoeken"/>{/*input--no-border*/}
          <button className="button" type="submit">&#128269;</button>{/*button--no-border*/}
        </div>
      </div>

    );
  }
}

export default MenuSearch;
