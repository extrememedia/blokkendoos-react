import React from 'react';
import { Link } from 'react-router-dom';
import MenuBrand from './MenuBrand';
import MenuList from './MenuList';
import MenuToggle from './MenuToggle';
import MenuSide from './MenuSide/MenuSide';


class Menu extends React.Component{

  render() {
    return (
			<div className="menu">
			  <div className="menu__bar">
			    <div className="menu__container">
			      <MenuBrand/>
            <MenuList/>
            <MenuToggle />
            <MenuSide/>
			    </div>
			  </div>
			</div>
		);
  }
}

export default Menu;
