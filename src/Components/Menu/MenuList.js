import React from 'react';
// import { Link } from 'react-router-dom';

class MenuList extends React.Component{

  render() {
    return (
      <React.Fragment>
        <ul className="menu-list">
          <li className="list__item">
            <a className="menu-list__link" href="#" title="Pagina 1">Pagina 1</a>
            <ul className="menu-list__submenu">
              <li className="list__item">
                <a className="menu-list__link" href="#" title="Pagina 1.1">Pagina 1.1</a>
              </li>
              <li className="list__item">
                <a className="menu-list__link" href="#" title="Pagina 1.2">Pagina 1.2</a>
              </li>
              <li className="list__item">
                <a className="menu-list__link" href="#" title="Pagina 1.3">Pagina 1.3</a>
              </li>
            </ul>
          </li>
          <li className="list__item">
            <a className="menu-list__link" href="#" title="Pagina 2">Pagina 2</a>
          </li>
          <li className="list__item">
            <a className="menu-list__link" href="#" title="Pagina 3">Pagina 3</a>
          </li>
        </ul>
      </React.Fragment>
    );
  }
}

export default MenuList;
