import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

class CardBody extends React.Component{

  render(){
    return <div className="card__body">
        <h3>{this.props.card.title}</h3>
        <p>{this.props.card.text}</p>
      </div>
    }}

CardBody.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string
};

export default CardBody;
