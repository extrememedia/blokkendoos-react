import React from 'react';
import { Link } from 'react-router-dom';
import CardCalendar from './CardCalendar';
import CardHeader from './CardHeader';
import CardBody from './CardBody';
import PropTypes from 'prop-types';


class Card extends React.Component{

  render(){
    return <React.Fragment>
        <a className="card card--responsive  card--shadow grid__item" href="#" title="#">
          <CardHeader hasCalendar={this.props.card.hasCalendar} month={this.props.card.month} day={this.props.card.day}/>
          <CardBody card={this.props.card}/>
        </a>
      </React.Fragment>;
    }}

Card.propTypes = {
  cards: PropTypes.array,
  month: PropTypes.string,
  day: PropTypes.string
};

export default Card;
