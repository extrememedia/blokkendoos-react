import React from 'react';
import { Link } from 'react-router-dom';
import Card from './Card';
import PropTypes from 'prop-types';

class CardContainer extends React.Component{

  state = {
    cards: [
      {
        title: 'Card1',
        text: 'Lorem ipsum',
        hasCalendar: true,
        month: 'Jan',
        day: '6'
      },
      {
        title: 'Card2',
        text: 'impossibru',
        hasCalendar: false,
        month: 'Feb',
        day: '23'
      },
      {
        title: 'Card3',
        text: 'do u know da wae',
        hasCalendar: true,
        month: 'Dec',
        day: '13'
      },
    ]
  };

  render() {
    return (
      <div className="section">
        <div className="container">
          <h2 className="section__title text-center">Agenda</h2>
          <div className="grid grid--3-for-min-tablet-landscape">
            {this.state.cards.map((card, index) => {
                return <Card card={card} key={`card-${index}`}/>
              }
            )}
          </div>
        </div>
      </div>
    );
  }
}


export default CardContainer;
