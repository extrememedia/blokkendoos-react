import React from 'react';
import { Link } from 'react-router-dom';
import CardCalendar from './CardCalendar';

class CardHeader extends React.Component{


  render() {

    var cardBG = {
      backgroundColor: '#eee',
      backgroundImage: 'url(http://via.placeholder.com/400x300)'
    };

    return <div className="card__image" style={cardBG}>
        {this.props.hasCalendar?<CardCalendar month={this.props.month} day={this.props.day} />:""}
      </div>
    }}

export default CardHeader;
