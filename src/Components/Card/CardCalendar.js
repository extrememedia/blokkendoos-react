import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';


class CardCalendar extends React.Component{

  render() {
    return (
      <div className="card__calendar-icon">
        <div className="calendar-icon__month">{this.props.month}</div>
        <div className="calendar-icon__day">{this.props.day}</div>
      </div>
    );
  }
}

CardCalendar.propTypes = {
  month: PropTypes.string,
  day: PropTypes.string
};

export default CardCalendar;
