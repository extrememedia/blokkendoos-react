import React from 'react';
import { Link } from 'react-router-dom';
import ContactForm from './ContactForm';
import ContactInfo from './ContactInfo';

class Contact extends React.Component{

  render() {
    return (
      <div className="section">
        <div className="container">
          <div className="grid grid--9-3">
            <div className="grid__item grid__item--left">
              <ContactForm/>
            </div>
            <div className="grid__item grid__item--right">
              <ContactInfo/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Contact;
