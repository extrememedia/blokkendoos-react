import React from 'react';
import { Link } from 'react-router-dom';

class ContactInfo extends React.Component{

  render() {
    return (
      <React.Fragment>
        <h2 className="widgettitle">Contactgegevens</h2>
        <ul className="fa-ul">
          <li>Extreme Media</li>
          <li>De Schans 1802<br />8231 KA Lelystad</li>
          <li><a href="#">info@extrememedia.nl</a></li>
          <li>0320 22 35 10</li>
        </ul>
      </React.Fragment>
    );
  }
}

export default ContactInfo;
