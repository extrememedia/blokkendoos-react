import React from 'react';
import { Link } from 'react-router-dom';

class ContactForm extends React.Component{

  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    const data = new FormData(event.target);

    fetch('/api/form-submit-url', {
      method: 'POST',
      body: data,
    });
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>

        <label htmlFor="username">Je naam (verplicht)</label>
        <input id="username" name="username" type="text" className="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"/>

        <label htmlFor="email">Je e-mail (verplicht)</label>
        <input id="email" name="email" type="email" className="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"/>

        <label htmlFor="subject">Onderwerp</label>
        <input id="subject" name="subject" type="text" className="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"/>

        <label htmlFor="message">Je bericht</label>
        <textarea className="wpcf7-form-control wpcf7-textarea" cols="40" name="message" rows="10" aria-label="message"></textarea>

        <button className="wpcf7-form-control wpcf7-submit">Verzenden</button>

      </form>
    );
  }
}
export default ContactForm;
