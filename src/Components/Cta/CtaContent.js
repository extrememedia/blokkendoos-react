import React from 'react';
import { Link } from 'react-router-dom';

class CtaContent extends React.Component{

  render() {
    return (
      <div className="cta">
        <div className="cta__description">
          <h2 className="cta__title">Lorem ipsum dolor sit amet</h2>
          <p className="cta__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
        <div className="cta__actions">
          <a className="link link--primary" href="#" title="#">Lorem ipsum dolor ➔</a>
        </div>
      </div>
    );
  }
}


export default CtaContent;
