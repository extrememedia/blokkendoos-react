import React from 'react';
import { Link } from 'react-router-dom';
import CtaContent from './CtaContent'

class Cta extends React.Component{

  render() {
    return (
      <div className="section section--bg">
        <div className="container">
          <CtaContent/>
        </div>
      </div>
    );
  }
}

export default Cta;
