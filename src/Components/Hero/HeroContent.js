import React from 'react';
// import { Link } from 'react-router-dom';

class HeroContent extends React.Component{

  render() {
    return (
      <div className="hero__content">
        <h1 className="hero__title">Lorem ipsum dolor</h1>
        <p className="hero__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
        <div className="hero__contact">
          <a className="button button--primary" href="#" title="Lees meer">Neem contact op</a>
          <a className="hero__phone" href="tel:+31 320 22 35 10" title="Bel +31 320 22 35 10">&#9742; +31 320 22 35 10</a>
        </div>
      </div>
    );
  }
}

export default HeroContent;
