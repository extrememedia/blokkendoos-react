import React from 'react';
import { Link } from 'react-router-dom';
import HeroContent from './HeroContent';

class Hero extends React.Component{

  render() {
    return (
      <div className="hero hero--overlay" style={{backgroundImage: 'url(http://via.placeholder.com/1920x1080)'}}>
        <div className="container">
          <HeroContent/>
        </div>
      </div>
    );
  }
}

export default Hero;
